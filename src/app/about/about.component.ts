import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  isReqQuotation=true;
  isQuickLinks=false;
  employeeService: any;
  constructor() { }

  ngOnInit(): void {
  }

  

   goReqQuotation(){
    this.isReqQuotation=true;
    this.isQuickLinks=false;
  }

    goQuickLinks(){
      this.isReqQuotation=false;
      this.isQuickLinks=true;
    }

    DeleteEmployee(id:number)
    {
      this.employeeService.deleteEmployee(id)
      .subscribe( 
        (data) =>{
          console.log(data);
          this.ngOnInit();
        }),
        err => {
          console.log("Error");
        }   
    }
}
