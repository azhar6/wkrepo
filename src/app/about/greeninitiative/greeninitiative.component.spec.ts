import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GreeninitiativeComponent } from './greeninitiative.component';

describe('GreeninitiativeComponent', () => {
  let component: GreeninitiativeComponent;
  let fixture: ComponentFixture<GreeninitiativeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GreeninitiativeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GreeninitiativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
