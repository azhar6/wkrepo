import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  isReqQuotation=true;
  isQuickLinks=false;
  mySubscription: any;
  //router:Router;
  
  // reloadCurrentRoute() {
  //   let currentUrl = this.router.url;
  //   this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
  //       this.router.navigate([currentUrl]);
  //       console.log(currentUrl);
  //   });
  // }

  ngOnInit(): void {
  }

  goReqQuotation(){
    this.isReqQuotation=true;
    this.isQuickLinks=false;
  }

    goQuickLinks(){
      this.isReqQuotation=false;
      this.isQuickLinks=true;
    }
}
