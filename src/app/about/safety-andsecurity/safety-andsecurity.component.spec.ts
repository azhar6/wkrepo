import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SafetyANDsecurityComponent } from './safety-andsecurity.component';

describe('SafetyANDsecurityComponent', () => {
  let component: SafetyANDsecurityComponent;
  let fixture: ComponentFixture<SafetyANDsecurityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SafetyANDsecurityComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SafetyANDsecurityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
