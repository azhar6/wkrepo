import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GreeninitiativeComponent } from './about/greeninitiative/greeninitiative.component';
import { HistoryComponent } from './about/history/history.component';
import { PhilosophyComponent } from './about/philosophy/philosophy.component';
import { SafetyANDsecurityComponent } from './about/safety-andsecurity/safety-andsecurity.component';
import { UsefulLinksComponent } from './about/useful-links/useful-links.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ApplyComponent } from './careers/apply/apply.component';
import { CareersComponent } from './careers/careers.component';
import { JobopeningsComponent } from './careers/jobopenings/jobopenings.component';
import { ProfilesComponent } from './careers/profiles/profiles.component';
import { ContactComponent } from './contact/contact.component';
import { ClientsayComponent } from './home/clientsay/clientsay.component';
import { FooterComponent } from './home/footer/footer.component';
// import { HomeComponent } from './home/home.component';
import { OurteamComponent } from './home/ourteam/ourteam.component';
import { Home1Component } from './home1/home1.component';
import { Nav1Component } from './nav1/nav1.component';
import { OurservicesComponent } from './ourservices/ourservices.component';
import { OvertheroadComponent } from './services/overtheroad/overtheroad.component';
import { RepairshopComponent } from './services/repairshop/repairshop.component';
import { ServicesComponent } from './services/services.component';
import { SliderComponent } from './slider/slider.component';
import { TrackComponent } from './track/track.component';
import { CertificationsComponent } from './whyus/certifications/certifications.component';
import { SpecialitiesComponent } from './whyus/specialities/specialities.component';
import { TechnologyComponent } from './whyus/technology/technology.component';
import { WhyusComponent } from './whyus/whyus.component';

const routes: Routes = [
  //{
  // path: '',
  // component: HomeComponent,     // this is the component with the <router-outlet> in the template
  // children: [
  {path:'' , component: Home1Component},  
  
  {path:'aboutus' , component:AboutusComponent},
  {path:'services' , component:ServicesComponent},
  {path:'whyus' , component:WhyusComponent},
  {path:'track' , component:TrackComponent},
  {path:'careers' , component:CareersComponent},
  {path:'slider' , component:SliderComponent},
  {path:'ourservices' , component:OurservicesComponent},
  {path:'footer' , component:FooterComponent},
  {path:'ourteam' , component:OurteamComponent},
  {path:'clientsay' , component:ClientsayComponent},
 {path:'history' , component:HistoryComponent},
 {path:'philosophy' , component:PhilosophyComponent},
 {path:'safetyANDsecurity' , component:SafetyANDsecurityComponent},
 {path:'greeninitiative' , component:GreeninitiativeComponent},
 {path:'usefulLinks' , component:UsefulLinksComponent},
 {path:'overtheroad' , component:OvertheroadComponent},
 {path:'repairshop' , component:RepairshopComponent},
 {path:'specialities' , component:SpecialitiesComponent},
 {path:'technology' , component:TechnologyComponent},
 {path:'certifications' , component:CertificationsComponent},
 {path:'profiles' , component:ProfilesComponent},
 {path:'apply' , component:ApplyComponent},
 {path:'jobopenings' , component:JobopeningsComponent},
 {path:'contact' , component:ContactComponent},
 {path:'home1', component:Home1Component},
 {path:'nav1', component:Nav1Component},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
