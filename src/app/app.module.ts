import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ServicesComponent } from './services/services.component';
import { WhyusComponent } from './whyus/whyus.component';
import { CareersComponent } from './careers/careers.component';
import { TrackComponent } from './track/track.component';

import { SliderComponent } from './slider/slider.component';
import { OurservicesComponent } from './ourservices/ourservices.component';
import { FooterComponent } from './home/footer/footer.component';
import { OurteamComponent } from './home/ourteam/ourteam.component';
import { ClientsayComponent } from './home/clientsay/clientsay.component';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';
import { HistoryComponent } from './about/history/history.component';
import { PhilosophyComponent } from './about/philosophy/philosophy.component';
import { SafetyANDsecurityComponent } from './about/safety-andsecurity/safety-andsecurity.component';
import { GreeninitiativeComponent } from './about/greeninitiative/greeninitiative.component';
import { UsefulLinksComponent } from './about/useful-links/useful-links.component';
import { OvertheroadComponent } from './services/overtheroad/overtheroad.component';
import { RepairshopComponent } from './services/repairshop/repairshop.component';
import { SpecialitiesComponent } from './whyus/specialities/specialities.component';
import { CertificationsComponent } from './whyus/certifications/certifications.component';
import { TechnologyComponent } from './whyus/technology/technology.component';
import { ProfilesComponent } from './careers/profiles/profiles.component';
import { ApplyComponent } from './careers/apply/apply.component';
import { JobopeningsComponent } from './careers/jobopenings/jobopenings.component';
import { ContactComponent } from './contact/contact.component';
import { Home1Component } from './home1/home1.component';
import { LatestNewsComponent } from './latest-news/latest-news.component';
import { SponsorComponent } from './sponsor/sponsor.component';
import { Nav1Component } from './nav1/nav1.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ServicesComponent,
    WhyusComponent,
    CareersComponent,
    TrackComponent,
    TechnologyComponent,
    SliderComponent,
    OurservicesComponent,
    FooterComponent,
    OurteamComponent,
    ClientsayComponent,
    NavigationbarComponent,
    HistoryComponent,
    PhilosophyComponent,
    SafetyANDsecurityComponent,
    GreeninitiativeComponent,
    UsefulLinksComponent,
    OvertheroadComponent,
    RepairshopComponent,
    SpecialitiesComponent,
    CertificationsComponent,
    ProfilesComponent,
    ApplyComponent,
    JobopeningsComponent,
    ContactComponent,
    Home1Component,
    LatestNewsComponent,
    SponsorComponent,
    Nav1Component,
    AboutusComponent
  ],
  imports: [
    
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AgmCoreModule.forRoot({
     
      apiKey: 'AIzaSyAvcDy5ZYc2ujCS6TTtI3RYX5QmuoV8Ffw'
    })
  ],
 
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
