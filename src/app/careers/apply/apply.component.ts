import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html',
  styleUrls: ['./apply.component.css']
})
export class ApplyComponent implements OnInit {
  isApply = true;
  isReqQuotation = false;
  isQuickLinks = false;
  constructor() { }

  ngOnInit(): void {
  }

  goApply() {
    this.isApply=true;
    this.isReqQuotation = false;
    this.isQuickLinks = false;
  }

  goReqQuotation(){
    this.isApply=false;
    this.isReqQuotation=true;
    this.isQuickLinks=false;
  }

  goQuickLinks(){
      this.isApply=false;
      this.isReqQuotation=false;
      this.isQuickLinks=true;
    }
}
