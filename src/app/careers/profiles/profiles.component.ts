import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {
  isAboutWheelking = true;
  isCareers = false;
  isReqQuotation = false;
  isQuickLinks = false;
  constructor() { }

  ngOnInit(): void {
  }

  goAboutWheelking() {
    this.isAboutWheelking=true;
    this.isCareers=false;
    this.isReqQuotation = false;
    this.isQuickLinks = false;
  }

  goReqQuotation(){
    this.isAboutWheelking=false;
    this.isCareers=false;
    this.isReqQuotation=true;
    this.isQuickLinks=false;
  }

  goQuickLinks(){
      this.isAboutWheelking=false;
      this.isCareers=false;
      this.isReqQuotation=false;
      this.isQuickLinks=true;
    }

    goCareers(){
      this.isAboutWheelking=false;
      this.isCareers=true;
      this.isReqQuotation=false;
      this.isQuickLinks=false;
    }
}
