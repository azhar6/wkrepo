import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  isApply = true;
  isReqQuotation = false;
  isQuickLinks = false;
  isQuickLinks2 = false;

  lat = 43.53250369240992;
  lng = -79.88634812496274;
  
  constructor() { }

  ngOnInit(): void {
  }

  goApply() {
    this.isApply=true;
    this.isReqQuotation = false;
    this.isQuickLinks = false;
  this.isQuickLinks2 = false;

  }

  goReqQuotation(){
    this.isApply=false;
    this.isReqQuotation=true;
    this.isQuickLinks=false;
  this.isQuickLinks2 = false;

  }

  goQuickLinks(){
      this.isApply=false;
      this.isReqQuotation=false;
      this.isQuickLinks=true;
  this.isQuickLinks2 = false;
    }

    goQuickLinks2(){
      this.isApply=false;
      this.isReqQuotation=false;
      this.isQuickLinks=false;
  this.isQuickLinks2 = true;
    }
}
