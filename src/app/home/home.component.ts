import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
 
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // isShow=false;
  isHome=true;
  isMenu=false;
  isTrash=false;
  
  constructor(private router: Router) { 
   
  }
  onShow(){
   
    this.router.navigate(['/overtheroad']);
    
  }
  onRepair(){
   
    this.router.navigate(['/repairshop']);
    
  }
  
  ngOnInit(): void {
  }

  goHome(){
    this.isHome=true;
    this.isMenu=false;
    this.isTrash=false;

  }
  goMenu(){
    this.isHome=false;
    this.isMenu=true;
    this.isTrash=false;

  }
  goTrash(){
    this.isHome=false;
    this.isMenu=false;
    this.isTrash=true;


  }
  
  
}

