import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav1',
  templateUrl: './nav1.component.html',
  styleUrls: ['./nav1.component.css']
})
export class Nav1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  header_variables =false;
    @HostListener("document:scroll")
    scrollfunction() {
      // var element = document.createElement('img');
      // element.setAttribute('src', options.url);

      if(document.body.scrollTop > 100 || document.documentElement.scrollTop > 400) {
        this.header_variables=true;

      }else{
        this.header_variables=false;
      }
    }
    
}
