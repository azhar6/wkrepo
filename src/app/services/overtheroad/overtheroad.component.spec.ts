import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OvertheroadComponent } from './overtheroad.component';

describe('OvertheroadComponent', () => {
  let component: OvertheroadComponent;
  let fixture: ComponentFixture<OvertheroadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OvertheroadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OvertheroadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
