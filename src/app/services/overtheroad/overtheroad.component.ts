import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-overtheroad',
  templateUrl: './overtheroad.component.html',
  styleUrls: ['./overtheroad.component.css']
})
export class OvertheroadComponent implements OnInit {

  isNetwork = true;
  isTruckload=false;
  isCapacity=false;
  isSpecialisedService=false;
  isReqQuotation=false;
  isQuickLinks=false;
  isLTLExpress=false;
  isOutsourcing=false;
  isCustomerService=false;

  constructor() { }

  ngOnInit(): void {
  }

  goNetwork() {
    this.isNetwork=true;
    this.isTruckload=false;
    this.isCapacity=false;
    this.isReqQuotation=false;
    this.isQuickLinks=false;
    this.isSpecialisedService=false;
    this.isLTLExpress=false;
    this.isOutsourcing=false;
    this.isCustomerService=false;
  }

  goTruckload() {
    this.isNetwork=false;
    this.isTruckload=true;
    this.isCapacity=false;
    this.isReqQuotation=false;
    this.isQuickLinks=false;
    this.isSpecialisedService=false;
    this.isLTLExpress=false;
    this.isOutsourcing=false;
    this.isCustomerService=false;

  }

  goCapacity() {
    this.isNetwork=false;
    this.isTruckload=false;
    this.isCapacity=true;
    this.isReqQuotation=false;
    this.isQuickLinks=false;
    this.isSpecialisedService=false;
    this.isLTLExpress=false;
    this.isOutsourcing=false;
    this.isCustomerService=false;

  }

  goReqQuotation(){
    this.isNetwork=false;
    this.isTruckload=false;
    this.isCapacity=false;
    this.isReqQuotation=true;
    this.isQuickLinks=false;
    this.isSpecialisedService=false;
    this.isLTLExpress=false;
    this.isOutsourcing=false;
    this.isCustomerService=false;

  }

    goQuickLinks(){
      this.isNetwork=false;
      this.isTruckload=false;
      this.isCapacity=false;
      this.isReqQuotation=false;
      this.isQuickLinks=true;
    this.isSpecialisedService=false;
    this.isLTLExpress=false;
    this.isOutsourcing=false;
    this.isCustomerService=false;

    }

    goSpecialisedService(){
      this.isNetwork=false;
      this.isTruckload=false;
      this.isCapacity=false;
      this.isReqQuotation=false;
      this.isQuickLinks=false;
      this.isSpecialisedService=true;
    this.isLTLExpress=false;
    this.isOutsourcing=false;
    this.isCustomerService=false;

    }

    goisLTLExpress(){
      this.isNetwork=false;
      this.isTruckload=false;
      this.isCapacity=false;
      this.isReqQuotation=false;
      this.isQuickLinks=false;
      this.isSpecialisedService=false;
    this.isLTLExpress=true;
    this.isOutsourcing=false;
    this.isCustomerService=false;

    }

    goOutsourcing(){
      this.isNetwork=false;
      this.isTruckload=false;
      this.isCapacity=false;
      this.isReqQuotation=false;
      this.isQuickLinks=false;
      this.isSpecialisedService=false;
    this.isLTLExpress=false;
    this.isOutsourcing=true;
    this.isCustomerService=false;

    }

    goCustomerService(){
      this.isNetwork=false;
      this.isTruckload=false;
      this.isCapacity=false;
      this.isReqQuotation=false;
      this.isQuickLinks=false;
      this.isSpecialisedService=false;
    this.isLTLExpress=false;
    this.isOutsourcing=false;
    this.isCustomerService=true;

    }

}
