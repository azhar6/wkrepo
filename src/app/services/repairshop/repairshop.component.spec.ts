import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairshopComponent } from './repairshop.component';

describe('RepairshopComponent', () => {
  let component: RepairshopComponent;
  let fixture: ComponentFixture<RepairshopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepairshopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
