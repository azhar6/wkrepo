import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-repairshop',
  templateUrl: './repairshop.component.html',
  styleUrls: ['./repairshop.component.css']
})
export class RepairshopComponent implements OnInit {
  isGreeninitiative = true;
  isReqQuotation = false;
  isQuickLinks = false;
  constructor() { }

  ngOnInit(): void {
  }

  goGreeninitiative() {
    this.isGreeninitiative=true;
    this.isReqQuotation = false;
    this.isQuickLinks = false;
  }

  goReqQuotation(){
    this.isGreeninitiative=false;
    this.isReqQuotation=true;
    this.isQuickLinks=false;
  }

  goQuickLinks(){
      this.isGreeninitiative=false;
      this.isReqQuotation=false;
      this.isQuickLinks=true;
    }
}
