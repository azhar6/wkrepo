import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-certifications',
  templateUrl: './certifications.component.html',
  styleUrls: ['./certifications.component.css']
})
export class CertificationsComponent implements OnInit {
  isCTPAT = true;
  isCSA=false;
  isFAST=false;
  isPIP=false;
  isReqQuotation=false;
  isQuickLinks=false;

  constructor() { }

  ngOnInit(): void {
  }

  goCTPAT() {
    this.isCTPAT=true;
    this.isCSA=false;
    this.isReqQuotation=false;
    this.isQuickLinks=false;
    this.isFAST=false;
    this.isPIP=false;
  }

  goCSA() {
    this.isCTPAT=false;
    this.isCSA=true;
    this.isReqQuotation=false;
    this.isQuickLinks=false;
    this.isFAST=false;
    this.isPIP=false;
  }
  
  goFAST() {
    this.isCTPAT=false;
    this.isCSA=false;
    this.isReqQuotation=false;
    this.isQuickLinks=false;
    this.isFAST=true;
    this.isPIP=false;
  }
  
  goPIP() {
    this.isCTPAT=false;
    this.isCSA=false;
    this.isReqQuotation=false;
    this.isQuickLinks=false;
    this.isFAST=false;
    this.isPIP=true;
  }
  
  goQuickLinks() {
    this.isCTPAT=false;
    this.isCSA=false;
    this.isReqQuotation=false;
    this.isQuickLinks=true;
    this.isFAST=false;
    this.isPIP=false;
  }
  
  goReqQuotation() {
    this.isCTPAT=false;
    this.isCSA=false;
    this.isReqQuotation=true;
    this.isQuickLinks=false;
    this.isFAST=false;
    this.isPIP=false;
  }

}
