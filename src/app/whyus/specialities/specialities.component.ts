import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-specialities',
  templateUrl: './specialities.component.html',
  styleUrls: ['./specialities.component.css']
})
export class SpecialitiesComponent implements OnInit {
  isSpeciality = true;
  isReqQuotation = false;
  isQuickLinks = false;
  constructor() { }

  ngOnInit(): void {
  }

  goSpeciality() {
    this.isSpeciality=true;
    this.isReqQuotation = false;
    this.isQuickLinks = false;
  }

  goReqQuotation(){
    this.isSpeciality=false;
    this.isReqQuotation=true;
    this.isQuickLinks=false;
  }

  goQuickLinks(){
      this.isSpeciality=false;
      this.isReqQuotation=false;
      this.isQuickLinks=true;
    }
}
