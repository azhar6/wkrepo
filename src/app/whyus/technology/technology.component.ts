import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-technology',
  templateUrl: './technology.component.html',
  styleUrls: ['./technology.component.css']
})
export class TechnologyComponent implements OnInit {
  isFocuscontrol = true;
  isGPSweb = false;
  isReqQuotation = false;
  isQuickLinks = false;
  constructor() { }

  ngOnInit(): void {
  }

  goFocusControl() {
    this.isFocuscontrol=true;
    this.isGPSweb=false;
    this.isReqQuotation = false;
    this.isQuickLinks = false;
  }

  goReqQuotation(){
    this.isFocuscontrol=false;
    this.isGPSweb=false;
    this.isReqQuotation=true;
    this.isQuickLinks=false;
  }

  goQuickLinks(){
      this.isFocuscontrol=false;
      this.isGPSweb=false;
      this.isReqQuotation=false;
      this.isQuickLinks=true;
    }

    goGPSweb(){
      this.isFocuscontrol=false;
      this.isGPSweb=true;
      this.isReqQuotation=false;
      this.isQuickLinks=false;
    }
}
